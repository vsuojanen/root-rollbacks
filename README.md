# README #

### What is this repository for? ###

Optional initramfs scripts for trial-root scheme, root-updates and root-rollbacks in Debian/Ubuntu based distribution. 

For more information about the trial-root and update & rollback scheme see https://bitbucket.org/vsuojanen/root-updates

### How do I get set up? ###

Depencies

    initramfs-tools

cp client/$DISTRO/* /usr

With that done, you should have the following executable files in your target distribution /share directory.

initramfs-tools/scripts/init-bottom/parted

initramfs-tools/scripts/init-premount/parted

initramfs-tools/scripts/root-fs

initramfs-tools/scripts/root-rw

initramfs-tools/hooks/disktools

Regenerate the initramfs:

Make sure your target environment uses the new initramfs to boot:

* How to run tests
* Deployment instructions


### Who do I talk to? ###

Valtteri Suojanen

This is free software; it is licensed under the terms of the GNU General Public License. Some parts of the source code must retain also the original BSD-derived license and copyright notice