#!/bin/sh
#
# Copyright 2015, Valtteri Suojanen
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License with your
# Debian GNU system, in /usr/share/common-licenses/GPL.  If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

PREREQ=""

prereqs()
{
    echo "$PREREQ"
}

case $1 in
# get pre-requisites
prereqs)
    prereqs
    exit 0
    ;;
esac

for x in $(cat /proc/cmdline); do
        case "$x" in
          loop=*)
            # basename "${file##*/}"?
            LOOP="${x#loop=}"
            LOOP_FILE="${x##*/}"
            NAME="${LOOP%.*}"
	    ;;
	esac
done

value_find()
{
        egrep '^'"$1"'=' "$2" | sed -e 's/^.*=//g'
}


# Find boot partition
boot_resolve()
{
	host_part="$(device_find "$BOOT_DEVICE")"
	test -b "$host_part" || return 1

	echo "parted: $BOOT_DEVICE is attached to local device $host_part"
	return 0
}

# Find boot partition
root_resolve()
{
        root_part="$(device_find "$ROOT_DEVICE")"
        test -b "$root_part" || return 1

        echo "parted: $ROOT_DEVICE is attached to local device $root_part"
        return 0
}

device_root()
{
	echo "$1" | sed -r -e 's/[0-9]*$//'
}

device_part_num()
{

	echo "$1" | sed 's/^\/dev\/[s|x?v]d[a-z]//g'
}


boot_set()
{
	# Find boot partition and set boot flag
	boot_resolve || exit 1
	local device="$(device_root "$host_part")"
	local number="$(device_part_num "$host_part")"
	/sbin/parted "$device" set "$number" boot on

}

rollback()
{
	# switch the partition labels
	local label=$1
	local out=$2
	local device=$3
        if [ -n "$label" ]; then
                device="$(device_find "$label")"
                test -b "$device" || die "unable to find '$label'"
        else
                label="$(device_label "$device")"
                test -n "$label" || die "unable to find label for '$device'"
        fi
        if is_ro "$label"; then
                rw_enable "$device" || die "failed to enable read/write on '$device'"
        fi

        echo "parted: Switching partition label \"$label\" to \"$out\""

        case "$label" in
        BOOT-A|ROOT-A)
                /sbin/e2label "$device" "$out"
                ;;
        *)
                /sbin/e2label "$device" "$out"
                rw_disable "$device"
		sleep 5
                ;;
        esac

        echo "parted: \"$out\" is now local device $device"

}

rollback_tree()
{
	rollback "BOOT-C" "BOOT-U"
        # BOOT-C is spare device (passive)
        rollback "BOOT-B" "BOOT-C"
        # BOOT-B is boot device (active)
        rollback "BOOT-A" "BOOT-B"
        # BOOT-A is  update device (passive)
        rollback "BOOT-U" "BOOT-A"

	rollback "ROOT-C" "ROOT-U"
        # ROOT-C is spare device (passive)
        rollback "ROOT-B" "ROOT-C"
        # ROOT-B is root device (active)
        rollback "ROOT-A" "ROOT-B"
        # ROOT-A is update device (passive)
        rollback "ROOT-U" "ROOT-A"
}

### 2 ###
# Compare the downloaded MD5SUMS to the files on the boot partition
# The MD5SUMS should contain all the current MD5 hashes that are watched and compared in the update process
boot_check()
{
echo "Verifying the fingerprints for "MD5SUMS"..." 2>&1 | tee -a "$bootlog"

cd /boot
/bin/md5sum -c MD5SUMS > CSUM
while read line; do
	CSUM1=$line
	file="$(echo "$line" | sed -r -e 's/^.* //')"
        echo "Verifying $file..." 2>&1 | tee -a "$bootlog"
	CSUM2="$(grep "$file" CSUM | grep -v "OK$")"

	if [ -n "$CSUM2" ]; then
	  if is_root_image "$file"; then
	    ROOT_UPDATE=true
	  else
	    BOOT_UPDATE=true
	  fi

	  echo "$CSUM1" | /bin/md5sum -c 2> /dev/null | tee -a "$bootlog"
	fi
done < /boot/MD5SUMS
cd /

}

#Main
BOOT_DEVICE="BOOT-B"
ROOT_DEVICE="ROOT-B"
. /scripts/root-fs
. /scripts/root-rw
# Wait for all devices to become available
sleep 5

boot_resolve || exit 1
root_resolve || exit 1

### 1 ###
# mount current boot partition
# /root mountpoint should be reserved for the real root filesystem
# use /boot directory for the bootloader and configurations
#

mkdir -p /boot
bootdev=$(blkid -o device -t LABEL=BOOT-B)
test -b "$bootdev" || exit 1
mount -t ext4 "$bootdev" "/boot" || exit 1

mkdir -p /boot/images
rootdev=$(blkid -o device -t LABEL=ROOT-B)
test -b "$rootdev" || exit 1
mount -t ext4 "$rootdev" "/boot/images" || exit 1

. /scripts/functions

[ "$quiet" != "y" ] && log_begin_msg "Setting up root-updates"

if [ -r "/boot/parted.conf" ]; then
	bootconfig="/boot/parted.conf"

        success="$(value_find "success" "$bootconfig")"
        tries="$(value_find "tries" "$bootconfig")"

	mkdir -p /boot/.root-update
	bootlog="/boot/.root-update/boot.log"
	echo "# This file contains whatever information was provided by root-updates setting up this device" > "$bootlog" 2>&1

	### 2 ###
	if [ $tries -gt 0 ]; then
	  boot_check
	  : $(( tries = $tries - 1 ))
	  sed -i "/tries=.*$/s//tries=${tries}/" $bootconfig || exit 1

	  if [ -n "$ROOT_UPDATE" ]; then
		echo "The OS root filesystem has changed. Configuring new startup..." 2>&1 | tee -a "$bootlog"

                echo "Switching partition labels..." 2>&1 | tee -a "$bootlog"

		umount "/boot/images" 2>&1 | tee -a "$bootlog"
		sleep 5

		rollback "ROOT-C" "ROOT-U" 2>&1 | tee -a "$bootlog"
	        # ROOT-C is spare device (passive)
	        rollback "ROOT-B" "ROOT-C" 2>&1 | tee -a "$bootlog"
	        # ROOT-B is root device (active)
	        rollback "ROOT-A" "ROOT-B" 2>&1 | tee -a "$bootlog"
	        # ROOT-A is update device (passive)
	        rollback "ROOT-U" "ROOT-A" 2>&1 | tee -a "$bootlog"

		echo "Rebooting..." 2>&1 | tee -a "$bootlog"
		sleep 2
		{ reboot; } 2>&1 | tee -a "$bootlog"
		sleep 10

		echo "This text is printed at the end of the reboot and it stalled here, type \"reboot\" and press Enter"
		exit 1
	  fi

	  if [ -n "$BOOT_UPDATE" ]; then
                echo "The bootloader files have changed. Configuring new startup..." 2>&1 | tee -a "$bootlog"

                echo "Switching partition labels and setting up boot partiton..." 2>&1 | tee -a "$bootlog"

		sleep 5
		umount "/boot"
		sleep 5

		rollback "BOOT-C" "BOOT-U"
	        # BOOT-C is spare device (passive)
	        rollback "BOOT-B" "BOOT-C"
	        # BOOT-B is boot device (active)
	        rollback "BOOT-A" "BOOT-B"
	        # BOOT-A is  update device (passive)
	        rollback "BOOT-U" "BOOT-A"

		boot_set

                echo "Rebooting..."
		sleep 2
		{ reboot; }
		sleep 10

                echo "This text is printed at the end of the reboot and it stalled here, type \"reboot\" and press Enter"
		exit 1
	  fi
	fi

	### 3 ###
	if [ $success -eq 1 ] || [ $tries -gt 0 ]; then
		echo "Success! OS root filesystem ${NAME} is booting from ${ROOT}" 2>&1 | tee -a "$bootlog"
		umount "/boot/images" 2>&1 | tee -a "$bootlog"
		sleep 5

		exit 0
	fi

	### 4 ###
	if [ $success -eq 0 ] && [ $tries -le 0 ]; then
                echo "Boot failed. Configuring new startup..." 2>&1 | tee -a "$bootlog"

                echo "Switching partition labels and setting up boot partiton..." 2>&1 | tee -a "$bootlog"

		umount "/boot/images" 2>&1 | tee -a "$bootlog"
		sleep 5
		umount "/boot"
		sleep 5

		rollback_tree
		boot_set

                echo "Rebooting..."
		sleep 2
		{ reboot; }
		sleep 10

                echo "This text is printed at the end of the reboot and it stalled here, type \"reboot\" and press Enter"
		exit 1
	fi

	umount "$root_part"
	# before exit unmount rootdevice
fi

[ "$quiet" != "y" ] && log_end_msg
